Repository updates:

September 23 2014 - 6:10PM:

-Made everything use the userID instead of their username. This is more efficient and better for linking db's.
-Added a groups page. Right now its not very functionable but it will take the name of your band and create it in a table.
-There seems to be a problem when it links back to the home page. I believe the controller is just changing the view and not actually
changing the controller and model etc, this resulting in error's on the home page that we dont have when we are originally on
the home page. You can access this page (localhost/musiclear/index.php/groups)

