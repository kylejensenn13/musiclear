<div id="login_form">
	
	<?php if (isset($account_created)) { ?>
		<h3><?php echo $account_created; ?></h3>
		<?php } else { ?>
			<h1>Login, please.</h1>
			<?php } ?>
			
			<?php
			echo form_open('login/validate_credentials');
			echo form_input('username', 'Username');
			echo form_password('password', '', 'placeholder="Password" class="password"');
			echo form_submit('submit', 'Login');
			echo anchor('login/signup', 'Create Account');
			echo form_close();
			?>
</div>

   <style type="text/css">
   	
body {
	background: #b6b6b6;
	margin: 0;
	padding: 0;
	font-family: 'Artubus Slab', serif;
}

.login_form {
	width: 910px
	background:#AAC4C5;
	border: 1px solid white;
	margin: 100px auto 0;
	padding: 1em;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
}
h1, h2, h3, h4, h5 {
	margin-top: 0;
	text-align: center;
	color: #39898D;
	text-shadow: 0 1px 0 white;
}

h1 {
	font-size: 1.6em;
}

h3 {
	font-size 1.1em;
}

input[type=text], input[type=password] {
	font-family: 'Arbutus Slab', serif;
	font-size: 14px;
	color: #39898D;
	display: block;
	margin: 0 0 1em 0;
	width: 273px;
	border: 5px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	padding: 1em;
}

input[type=text]:focus, input[type=password]:focus {
	outline: none;
	box-shadow: 0px 0px 0px 2pt #549396;
}

input::-webkit-input-placeholder {
	color: #39898D;
}

input:-moz-placeholder {
	color: #39898D;
}
input:-ms-input-placeholder {
	color: #549396;
}

input[type=submit], form a {
	font-family: 'Arbutus Slab', serif;
	border: none;
	margin-right: 1em;
	padding: 6px;
	text-decoration: none;
	font-size: 14px;
	background: #549396;
	color: white;
	-moz-box-shadow: 0 1px 0 white;
	-webkit-box-shadow: 0 1px 0 white;
	box-shadow: 0 1px 0 white;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border-radius: 2px;
}

input[type=submit]:hover, form a:hover {
	background: #39898D;
	cursor: pointer;
}

.error {
	color: #393939;
	font-size: 15px;
}

	</style>
