<div id="band_form">
	<h1>Create a Music Group of your own!</h1>
	<?php
	echo form_open('bands/create_band');
	echo form_input('band_name', set_value('band_name', 'Band Name'));
	echo form_submit('submit', 'Start Creating Now');
	?>
	<?php echo validation_errors('<p class="error">'); ?>

</div>
<div id="bands_list">
	<h3>List of Bands:</h3>
	<?php 
	print $_SERVER["REQUEST_URI"];
	foreach($bands as $band) {?>
    <p><a href=<?php echo $_SERVER["REQUEST_URI"] . '/band_profile';?>> <?php echo $band->band_name; ?></p></a><br/>
	<?php }?>
</div>
