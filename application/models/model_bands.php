<?php

class Model_bands extends CI_Model 
{
	
	function create_band() {
		
		$bandname = $this->input->post('band_name');
		
		$new_band_data = array(
		'band_name' => $bandname);

		$insert = $this->db->insert('bands', $new_band_data);

		return $insert;
	}
	function getAllBandsData($data) {
		$query = $this->db->query('SELECT ' . $data . ' FROM bands');
		return $query->result();	
	}
	
	function addBandManager($band, $user) {
		$new_band_manager = array('band_ID' => $band, 'user_ID' => $user);
		
		$insert = $this->db->insert('band_managers', $new_band_manager);
		
		return $insert;
		
	}

	function check_if_band_exists($bandname) {
		
		$this->db->where('band_name', $bandname);
		$result = $this->db->get('bands');
		
		if ($result->num_rows() > 0) {
			return FALSE; //Taken
		} else {
			return TRUE; //Available
		}
	}
	
	function getBandData($data, $band) {
		
		$this->db->select($data);
		$this->db->from('bands');
		$this->db->where('band_name', $band);
		$query = $this->db->get();

		foreach ($query->result() as $row) {
			return $row->$data;
		}
	}

	function addBandMember($band, $user) {
		$new_member_data = array('band_ID' => $band, 'user_ID' => $user);
		
		$insert = $this->db->insert('band_members', $new_member_data);
		return $insert;
	}
	
	function getBandMembers($band) {
		$data = 'user_ID';
		$this->db->select($data);
		$this->db->from('band_members');
		$this->db->where('band_ID', $band);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			return $row->$data;
		}
	}
	
}
	

