<?php
	class Model_users extends CI_Model {
		function __construct() {
			
			parent::__construct(); //Call the Model constructor
		}
		
		function getFirstNames() {
			$query = $this->db->query('SELECT first_name FROM users'); // returns an object
			
			if ($query->num_rows() > 0) { //returns true if more than one row was found
				return $query->result(); //returns an array of objects
			} else {
				return NULL;
			}
		}
		
		function getUsers() {
			$query = $this->db->query('SELECT * FROM users');
			
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return NULL;
			}
		}
		function getUserID($user) {
			$data = 'user_ID';
			$this->db->select($data);
			$this->db->from('users');
			$this->db->where('username', $user);
			$query = $this->db->get();
			
			foreach ($query->result() as $row) {
				return $row->$data;
			}
		}
		
		function getUserData($data) 
		{
			$user = $this->model_users->getUserID($this->session->userdata('username'));
			
			$this->db->select($data);
			$this->db->from('users');
			$this->db->where('user_ID', $user); 
			$query = $this->db->get();

    		foreach ($query->result() as $row)
			{
   				return $row->$data;
   			}
		}		
	}
?>