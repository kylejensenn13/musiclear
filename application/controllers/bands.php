<?php

class Bands extends CI_Controller {
	
	function index() {
		
		$data = array();
    	$this->load->model('model_bands'); // load the model
    	$bands = $this->model_bands->getAllBandsData('band_name'); // use the method
    	$data['bands'] = $bands; // put it inside a parent array

		$this->load->view('includes/header');
		$this->load->view('band_view', $data);
		$this->load->view('includes/footer');
		
		$this->create_band();
	}
	function create_band() {
		$this->load->library('form_validation');
		$this->load->model('model_users');
		
		// validation rules
		$this->form_validation->set_rules('band_name', 'Band Name', 'trim|required|min_length[4]|max_length[50]');

		if ($this->form_validation->run() == FALSE) { //didnt validate
			
			$this->load->view('includes/header');
			$this->load->view('band_view');
			$this->load->view('includes/footer');
			
			
		} else {
			$this->load->model('model_bands');
			
			if($query = $this->model_bands->create_band()) {
				//$data['account_created'] = 'Your account has been created.<br/><br/>You may now sign in.';
				$this->model_bands->addBandManager($this->model_bands->getBandData('band_ID', $this->input->post('band_name')), $this->model_users->getUserData('user_ID'));
				$this->model_bands->addBandMember(23, $this->model_users->getUserData('user_ID'));
				
				$this->load->view('includes/header');
				$this->load->view('bandprofile_view');
				$this->load->view('includes/footer');
				
			} else {
				$this->load->view('includes/header');
				$this->load->view('bandprofile_view');
				$this->load->view('includes/footer');

			}
		} 
				
	}

	function band_profile() {

    	$this->load->model('model_bands'); // load the model
    	$data['band_name'] = $this->model_bands->getBandData('band_name', 'Eminem'); // use the method
    	
		$this->load->view('includes/header');
		$this->load->view('bandprofile_view', $data);	
		$this->load->view('includes/footer');	
	}
	
	function check_if_band_exists($requested_band) {
		$this->load->model('model_bands');
		
		$band_name_available = $this->model_bands->check_if_band_exists($requested_band);
		
		if ($band_name_available) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
}
