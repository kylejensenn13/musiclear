<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {
		$this->home();
	}
	
	public function home() {
		$this->load->model('model_users');
		
		$data['firstnames'] = $this->model_users->getFirstNames();
		// just stored the array of objects into $data['firstnames] it will be accessible in the views as $firstnames		
		$data['users'] = $this->model_users->getUsers();
		
		$this->load->view('home_view', $data);	
	}
	public function logout() {
   		$this->session->unset_userdata('is_logged_in');
  		session_destroy();
   		redirect('login', 'refresh');
	}
}

