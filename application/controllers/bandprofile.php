<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bandprofile extends CI_Controller {
	
	public function index() {
		$this->main();
	}
	
	public function main() {
		$this->load->model('model_bands');

		$this->load->view('bandprofile_view');	
	}
	public function logout() {
   		$this->session->unset_userdata('is_logged_in');
  		session_destroy();
   		redirect('login', 'refresh');
	}
}
?>
